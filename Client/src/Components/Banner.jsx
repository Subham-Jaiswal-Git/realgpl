import React from "react";
import "../CSS/Banner.css";

const Banner =()=>{
    return(
        <div>
        <div className="container-fluid banner">
            <div className="row">
                <div className="col-12 col-md-4 col-sm-12">
                    <button className="button1">WPBakery & Elementor</button>
                    <h1 className="title1">Build a creative<br></br>shop &<br></br>website.</h1>
                    <p className="p1"><b>Quick and easy getting started</b><br></br>with carefully crafted ready-to-go templates.</p>
                </div>
                <div className="col-12 col-md-8 col-sm-12 banner-image">
                    <img src="img19.png" alt="..."></img>
                </div>
            </div>
        </div>
        
        </div>

    );
};

export default Banner;