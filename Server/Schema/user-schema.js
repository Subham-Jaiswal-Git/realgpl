import mongoose from "mongoose";
// import autoIncrement from 'mongoose-auto-increment';

const signupSchema = mongoose.Schema({
    name:String,
    email:String,
    password:String,
    confirmpassword:String
});

// autoIncrement.initialize(mongoose.connection);
// signupSchema.plugin(autoIncrement.plugin,'signup');

const signup = mongoose.model('usersignup', signupSchema);

export default signup;