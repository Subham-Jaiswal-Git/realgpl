import mongoose from "mongoose";

const Connection = async(username, password) =>{

    const URL = `mongodb://${username}:${password}@ac-bcgewdf-shard-00-00.vosxgm2.mongodb.net:27017,ac-bcgewdf-shard-00-01.vosxgm2.mongodb.net:27017,ac-bcgewdf-shard-00-02.vosxgm2.mongodb.net:27017/?ssl=true&replicaSet=atlas-y4tejm-shard-0&authSource=admin&retryWrites=true&w=majority`;
    try{
        await mongoose.connect(URL, {useUnifiedTopology:true, useNewUrlParser:true, });
        console.log("Db Connection Successfully");
    }catch(error){
        console.log('Connection Error with Database', error);
    }
}

export default Connection;