import express from "express";
import dotenv from 'dotenv';
import cors from 'cors';
import Connection from "./Database/db.js";
import router from "./Routes/Route.js";
// import Routes from "./Routes/Route.js"
import bodyParser from 'body-parser';

const app = express();
dotenv.config();
app.use(bodyParser.json({extended:true}));
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.json())
app.use(cors());
app.use('/', router);


const PORT = 8000;
const username = process.env.DB_USERNAME;
const password = process.env.DB_PASSWORD;


Connection(username, password);

app.listen(PORT, () =>{
    console.log('Server is Running Successfull on port Number 8000');
})