import signup from "../Schema/user-schema.js";

export const addUser = async(request,respond)=>{
    const user = request.body; 
    
    const newUser = signup(user);
    try{
        await newUser.save();
        respond.status(201).json(newUser);
    }catch(error){
        respond.status(409).json({message:error.message});
    }

}
// export const getUsers = async(request,response) =>{
//     try{
//         const users = await User.find();
//         response.status(200).json(users);
//     }catch(error){
//         response.status(404).json({message:error.message});
//     }
// }