import express, {Router} from 'express';
import { addUser } from '../Controller/userController.js';
// import {addUser,getUser} from '../Controller/user-controller';

const router = express.Router();

router.post('/signup',addUser);
// router.get('/all', getUsers);

export default router;